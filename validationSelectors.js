// @ts-check
// @ts-ignore
/** @type {import('./types')} */

import _ from 'lodash';
import { getStoreFieldName } from '../index';
import { canShowErrorMessages } from '../messages';

/**
 * Get the validation store slice from the root redux state
 * @param {any} state
 */
export const getValidationStore = state => _.get(state, [getStoreFieldName(), 'validationStore']);

/**
 * Return all validation results for given page
 * @param {any} state
 * @param {string} topic
 * @param {string} page
 * @returns {Object<string, ValidationResult>}
 */
export const getValidationResultsForPage = (state, topic, page) =>
	_.get(getValidationStore(state), [topic, page, 'validationResultsMap'], {});

/**
 * Get the status of the last validation run for a page
 * @param {any} state
 * @param {string} topic
 * @param {string} page
 * @returns {boolean} - True if the last validation run for the page passed
 */
export const getAllValidationsPassedForPage = (state, topic, page) => {
	return _.get(getValidationStore(state), [topic, page, 'allValidationsPassedForPage']);
};

/**
 * @param {any} state
 * @param {string} topic
 * @param {string} page
 * @param {string} uid - UID for the field
 * @returns {Partial<ValidationResult>}
 *
 */
export const getValidationResultForFieldUid = (state, topic, page, uid) => {
	return _.get(getValidationStore(state), [topic, page, 'validationResultsMap', uid], {});
};

/**
 * Returns true if the field has a validation message targeting it
 *
 * @param {any} state
 * @param {string} topic
 * @param {string} page
 * @param {string} uid - The field uid to get messages for
 * @returns {boolean}
 */
export const getFieldHasValidationMessage = (state, topic, page, uid) => {
	const { validationMessage } = getValidationResultForFieldUid(state, topic, page, uid);
	return !_.isEmpty(validationMessage);
};

/**
 * Collects all validation messages from the store for given topic and page
 * @param {any} state
 * @param {string} topic
 * @param {string} page
 * @returns {ValidationMessage[]}
 */
export const getAllValidationMessagesForPage = (state, topic, page) => {
	if (canShowErrorMessages(state, topic, page)) {
		const validationResults = getValidationResultsForPage(state, topic, page);
		return _.reduce(
			_.values(validationResults),
			(acc, result) => {
				if (result.validationMessage) {
					acc.push(result.validationMessage);
				}
				return acc;
			},
			[]
		);
	}
	return [];
};
