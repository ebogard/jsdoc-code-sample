// @ts-check
// @ts-ignore
/** @type {import('./types')} */

import _ from 'lodash';
import * as actionTypes from '../store/actionTypes';
/**
 * @typedef {Object} TopicPageValidation
 * @property {Object} topic - Topic for the page
 * @property {Object} topic.page
 * @property {boolean} topic.page.allValidationsPassedForPage
 * @property {Object.<string, ValidationResult>} topic.page.validationResultsMap - A mapping of field uid to ValidationResult
 * @property {Object} lastSubmitFormValues
 */

// {Dictionary of functions} - Holds all the reducers
const reducers = {};

/**
 * Main reducer function that provides routing to the correct reducer
 */
export default (state = {}, action) => {
	const targetReducerFunc = reducers[action.type];

	// Execute reducer if it exists
	if (typeof targetReducerFunc === 'function') {
		return targetReducerFunc(state, action);
	}

	return state;
};

/**
 * Updates the validation store with new validation results
 * @param {any} state
 * @param {ValidationRunResults} action
 */
reducers[actionTypes.VALIDATION_RUN_RESULTS] = (state, action) => {
	const {
		topic,
		page,
		validationResults,
		allValidationsPassedForPage,
		storeResultSet,
	} = action.payload;

	if (!storeResultSet) {
		return state;
	}

	const newResultMap = _.reduce(
		validationResults,
		(acc, result) => {
			return _.set(acc, result.uid, result);
		},
		{}
	);
	return {
		...state,
		[topic]: {
			[page]: {
				allValidationsPassedForPage,
				validationResultsMap: newResultMap,
			},
		},
	};
};

/**
 * Reset the validation messaging data when the user navigates to a page
 */
reducers[actionTypes.GOTO_PAGE] = () => {
	return {};
};

/**
 * Reset the validation messaging data when the user naviates to a dialog
 */
reducers[actionTypes.OPEN_DIALOG] = reducers[actionTypes.GOTO_PAGE];
/**
 * Reset the validation messaging data when the user closes to a dialog
 */
reducers[actionTypes.CLOSE_DIALOG] = reducers[actionTypes.GOTO_PAGE];
