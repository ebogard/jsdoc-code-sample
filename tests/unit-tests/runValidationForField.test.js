// @ts-check
// @ts-ignore
/** @type {import('../../types')} */
import { runValidationForField } from '../../validationActions';
import maskTypes from '../../../masking/maskTypes';
import { messageTypes } from '../../../dismiss-message/messageTypes';
import { getInlineMessageData } from '../../../messages/messageUtility';

describe('runValidationForField', () => {
	it('should invalidate a required field with no value', () => {
		/** @type {ValidationField} */
		const field = {
			flattenedId: 'Somegroup.SomeID',
			caption: 'this field',
			id: 'Somegroup.SomeID',
			maskedValue: '',
			value: 'Field unmasked value',
			uid: '123',
			isFieldReadOnly: false,
			objectRef: '12345',
			required: '1',
		};
		/** @type {ValidationResult} */
		const expectedResult = {
			flattenedId: field.flattenedId,
			uid: field.uid,
			isValid: false,
			isRequiredValid: false,
			isFormatValid: false,
			isFieldValueAllowed: false,
		};
		expect(runValidationForField(field, [])(null, () => {})).toEqual(
			expect.objectContaining({
				...expectedResult,
				validationMessage: expect.objectContaining({
					message: 'this field is required',
				}),
			})
		);
	});
	it('should invalidate an SSN format mask with only 8 characters', () => {
		/** @type {ValidationField} */
		const field = {
			flattenedId: 'Somegroup.SomeID',
			caption: 'this field',
			id: 'Somegroup.SomeID',
			maskedValue: '123-45-678',
			value: '12345678',
			uid: '123',
			isFieldReadOnly: false,
			objectRef: '12345',
			required: '1',
			maskType: maskTypes.SSN,
		};
		/** @type {ValidationResult} */
		const expectedResult = {
			flattenedId: field.flattenedId,
			uid: field.uid,
			isValid: false,
			isRequiredValid: true,
			isFormatValid: false,
			isFieldValueAllowed: false,
		};
		expect(runValidationForField(field, [])(null, () => {})).toEqual(
			expect.objectContaining({
				...expectedResult,
				validationMessage: expect.objectContaining({
					message: 'this field must be 9 digits in length',
				}),
			})
		);
	});
	it('should invalidate a field with a blacklisted value', () => {
		/** @type {ValidationField} */
		const field = {
			flattenedId: 'Somegroup.SomeID',
			caption: 'this field',
			uid: '123',
			id: 'Somegroup.SomeID',
			maskedValue: 'foo',
			value: 'foo',
			isFieldReadOnly: false,
			objectRef: '12345',
			required: '1',
			[sensitive]BlackList: 'foo,bar',
		};
		/** @type {ValidationResult} */
		const expectedResult = {
			flattenedId: field.flattenedId,
			uid: field.uid,
			isValid: false,
			isRequiredValid: true,
			isFormatValid: true,
			isFieldValueAllowed: false,
		};
		expect(runValidationForField(field, [])(null, () => {})).toEqual(
			expect.objectContaining({
				...expectedResult,
				validationMessage: expect.objectContaining({
					message: 'this field cannot be foo',
				}),
			})
		);
	});
	it('should succeed if a field passes all validations', () => {
		/** @type {ValidationField} */
		const field = {
			flattenedId: 'Somegroup.SomeID',
			caption: 'this field',
			id: 'Somegroup.SomeID',
			maskedValue: '123-45-6789',
			value: '123456789',
			objectRef: '12345',
			isFieldReadOnly: false,
			uid: '123',
			required: '1',
			maskType: maskTypes.SSN,
			[sensitive]BlackList: 'someValue,anotherValue',
		};
		/** @type {BasicMessage} */
		const message = {
			targetFieldIdArray: ['SomeOtherGroup.SomeOtherId'],
			messageId: {
				refObject: '12345',
				messageName: 'error',
			},
			message: 'business rules message',
			type: messageTypes.ERROR,
			inlineMessage: getInlineMessageData(messageTypes.ERROR),
			dialogLevelData: {},
			rawMessageWithGuid: 'someguid',
		};
		/** @type {ValidationResult} */
		const expectedResult = {
			flattenedId: field.flattenedId,
			uid: field.uid,
			isValid: true,
			isRequiredValid: true,
			isFormatValid: true,
			isFieldValueAllowed: true,
		};
		expect(runValidationForField(field, [message])(null, () => {})).toEqual(expectedResult);
	});
});
