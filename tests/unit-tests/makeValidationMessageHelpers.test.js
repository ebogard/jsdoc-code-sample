import { messageTypes, maskTypes } from '../../../index';
import {
	makeValidationMessage,
	makeRequiredValidationMessage,
	makeFormatValidationMessage,
	makeBlacklistValidationMessage,
} from '../../validationActions';

describe('makeValidationMessage', () => {
	it('should make an invalid type validation message', () => {
		const flattenedId = 'FieldGroup.FieldName';
		const messageText = 'An example message';
		const message = makeValidationMessage(flattenedId, 'invalid', messageText);
		expect(message).toEqual({
			targetFieldIdArray: [flattenedId],
			messageId: expect.objectContaining({
				messageName: 'invalid',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
	it('should make a required type validation message', () => {
		const flattenedId = 'FieldGroup.FieldName';
		const messageText = 'An example message';
		const message = makeValidationMessage(flattenedId, 'required', messageText, 'required');
		expect(message).toEqual({
			targetFieldIdArray: [flattenedId],
			messageId: expect.objectContaining({
				messageName: 'required',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'required',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
});

describe('makeRequiredValidationMessage', () => {
	it('should make a required type validation message with default required message', () => {
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
		};
		const message = makeRequiredValidationMessage(field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'required',
			}),
			message: 'A field is required',
			displayMessage: 'A field is required',
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'required',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: 'A field is required',
		});
	});
	it('should use [sensitive]RequiredMessage property for the message', () => {
		const messageText = 'This field is super required';
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
			[sensitive]RequiredMessage: messageText,
		};
		const message = makeRequiredValidationMessage(field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'required',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'required',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
});

describe('makeFormatValidationMessage', () => {
	it('should make an invalid type format validation message with default format message', () => {
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
		};
		const message = makeFormatValidationMessage({}, field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'format',
			}),
			message: 'A field is invalid',
			displayMessage: 'A field is invalid',
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: 'A field is invalid',
		});
	});
	it('should use [sensitive]FormatMessage property for the message', () => {
		const messageText = 'This field is super invalid';
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
			[sensitive]FormatMessage: messageText,
		};
		const message = makeFormatValidationMessage({}, field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'format',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
	it('should use format mask maskType for the message', () => {
		const messageText = 'A field must be 9 digits in length';
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
			maskType: maskTypes.SSN,
		};
		const message = makeFormatValidationMessage({}, field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'format',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
});

describe('makeBlacklistValidationMessage', () => {
	it('should make a blacklist validation message with default blacklist message', () => {
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
			value: '8',
		};
		const message = makeBlacklistValidationMessage(field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'invalid',
			}),
			message: 'A field cannot be 8',
			displayMessage: 'A field cannot be 8',
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: 'A field cannot be 8',
		});
	});
	it('should use [sensitive]FormatMessage property for the message', () => {
		const messageText = 'This field is super blacklisted';
		const field = {
			flattenedId: 'FieldGroup.FieldName',
			caption: 'A field',
			[sensitive]BlackListMessage: messageText,
		};
		const message = makeBlacklistValidationMessage(field);
		expect(message).toEqual({
			targetFieldIdArray: [field.flattenedId],
			messageId: expect.objectContaining({
				messageName: 'invalid',
			}),
			message: messageText,
			displayMessage: messageText,
			type: messageTypes.ERROR,
			inlineMessage: expect.objectContaining({
				message: 'invalid',
			}),
			dialogLevelData: {},
			rawMessageWithGuid: messageText,
		});
	});
});
