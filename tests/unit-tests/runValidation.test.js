// @ts-check
// @ts-ignore
/** @type {import('../../types')} */
import configureStore from '../../../tests/configureStore';
import { runValidation } from '../../validationActions';
import maskTypes from '../../../masking/maskTypes';
import { VALIDATION_RUN_RESULTS } from '../../../store/actionTypes';
import {
	setupLoginMockResponse,
	setupResumeMockResponse,
	setupLoadPageMockResponse,
	generateSimpleGetPageResponseWithDefaults,
} from '../../../tests';
import { loadPage } from '../../../form';

const mockDispatch = jest.fn();

const topic = 'CarrierTestPage';
const page = 'TestPage';
const manuscript = 'asdf';

describe('runValidation', () => {
	beforeEach(() => {
		mockDispatch.mockClear();
		setupResumeMockResponse();
		setupLoginMockResponse();
	});
	it('should run validation over given fields and emit action with payload', () => {
		/** @type {ValidationField[]} */
		const fieldList = [
			{
				flattenedId: 'Somegroup.SomeID1',
				uid: '123',
				caption: 'this field',
				id: 'Somegroup.SomeID1',
				maskedValue: '123-45-6789',
				value: '123456789',
				objectRef: '12345',
				required: '1',
				isFieldReadOnly: false,
				maskType: maskTypes.SSN,
				[sensitive]BlackList: 'someValue,anotherValue',
			},
			{
				flattenedId: 'Somegroup.SomeID2',
				uid: '1234',
				caption: 'this field',
				id: 'Somegroup.SomeID2',
				maskedValue: '123456789',
				value: '123456789',
				objectRef: '12345',
				isFieldReadOnly: false,
				required: '1',
			},
			{
				flattenedId: 'Somegroup.SomeID3',
				uid: '12345',
				caption: 'this field',
				id: 'Somegroup.SomeID3',
				maskedValue: '',
				value: '',
				isFieldReadOnly: false,
				objectRef: '12345',
				required: '0',
			},
		];
		setupLoadPageMockResponse(topic, page, () =>
			generateSimpleGetPageResponseWithDefaults({
				topic,
				page,
				fields: fieldList,
			})
		);
		const store = configureStore();
		return loadPage({
			topic,
			page,
			manuscript,
		})(store.dispatch, store.getState)
			.then(() => {
				return runValidation(topic, page, fieldList)(mockDispatch, store.getState);
			})
			.then(results => {
				const { validationResults } = results;
				expect(validationResults).toHaveLength(3);
				expect(validationResults).toEqual(
					expect.arrayContaining([
						expect.objectContaining({
							flattenedId: 'Somegroup.SomeID1',
							isValid: true,
						}),
						expect.objectContaining({
							flattenedId: 'Somegroup.SomeID2',
							isValid: true,
						}),
						expect.objectContaining({
							flattenedId: 'Somegroup.SomeID3',
							isValid: true,
						}),
					])
				);
				expect(mockDispatch).toBeCalledWith(
					expect.objectContaining({
						type: VALIDATION_RUN_RESULTS,
						payload: {
							topic,
							page,
							allValidationsPassedForPage: true,
							validationResults: expect.arrayContaining([
								expect.objectContaining({
									flattenedId: 'Somegroup.SomeID1',
									uid: '123',
									isValid: true,
								}),
								expect.objectContaining({
									flattenedId: 'Somegroup.SomeID2',
									uid: '1234',
									isValid: true,
								}),
								expect.objectContaining({
									flattenedId: 'Somegroup.SomeID3',
									uid: '12345',
									isValid: true,
								}),
							]),
						},
					})
				);
			});
	});
	it('should ignore fields which are read only or wont render', () => {
		const store = configureStore();
		/** @type {ValidationField[]} */
		const fieldList = [
			// Read only
			{
				flattenedId: 'Somegroup.SomeID1',
				uid: '123',
				caption: 'this field',
				id: 'Somegroup.SomeID1',
				maskedValue: 'foo',
				value: 'foo',
				objectRef: '12345',
				required: '1',
				isFieldReadOnly: true,
			},
			// should show expression
			{
				uid: '1234',
				flattenedId: 'Somegroup.SomeID2',
				caption: 'this field',
				id: 'Somegroup.SomeID2',
				maskedValue: '123456789',
				value: '123456789',
				objectRef: '12345',
				required: '1',
				isFieldReadOnly: false,
				[sensitive]ShouldShowExpression: 'Somegroup.SomeID1 != foo',
			},
			// valid should show expression
			{
				uid: '12345',
				flattenedId: 'Somegroup.SomeID3',
				caption: 'this field',
				id: 'Somegroup.SomeID3',
				maskedValue: '123',
				value: '123',
				isFieldReadOnly: false,
				objectRef: '12345',
				required: '1',
				[sensitive]ShouldShowExpression: 'Somegroup.SomeID1 == foo',
			},
			// valid
			{
				uid: '123456',
				flattenedId: 'Somegroup.SomeID4',
				caption: 'this field',
				id: 'Somegroup.SomeID4',
				maskedValue: '123',
				value: '123',
				isFieldReadOnly: false,
				objectRef: '12345',
				required: '1',
			},
		];
		setupLoadPageMockResponse(topic, page, () =>
			generateSimpleGetPageResponseWithDefaults({
				topic,
				page,
				fields: fieldList,
			})
		);
		return loadPage({
			topic,
			page,
			manuscript,
		})(store.dispatch, store.getState).then(() => {
			return runValidation(topic, page, fieldList)(mockDispatch, store.getState).then(
				results => {
					expect(results.validationResults).toHaveLength(2);
				}
			);
		});
	});
});
