// @ts-check
// @ts-ignore
/** @type {import('./types')} */

import _ from 'lodash';
import { messageTypes, getInlineMessageData, getFormatMessage } from '../index';
import { getIsRequiredValidFunc, getIsFormatValidFunc } from '../masking';
import { getShouldFieldRender } from '../form';
import { VALIDATION_RUN_RESULTS } from '../store/actionTypes';

/**
 * Creates a {@link ValidationMessage} for a {@link ValidationResult}
 * @param {FlattenedID} flattenedId - Flattenend id for the field this message should target
 * @param {string} messageName - Identifier for the message being created
 * @param {string} messageText - Message text to display
 * @param {'required'} [inlineMessageType] - Set inline message text for required validations
 * @returns {ValidationMessage}
 */
export const makeValidationMessage = (flattenedId, messageName, messageText, inlineMessageType) => {
	return {
		targetFieldIdArray: [flattenedId],
		messageId: {
			refObject: flattenedId,
			messageName,
		},
		message: messageText,
		displayMessage: messageText,
		type: messageTypes.ERROR,
		inlineMessage: getInlineMessageData(messageTypes.ERROR, { subType: inlineMessageType }),
		dialogLevelData: {},
		// The GUID referred to below is the GUID passed up from Duck Creek.  Since this is a UI
		// generated message there isn't a GUID to apply and we just use the message itself.
		rawMessageWithGuid: messageText,
	};
};

/**
 * Makes a validation message for a required failure
 * @param {ValidationField} field - Field to make message for
 * @returns {ValidationMessage}
 */
export const makeRequiredValidationMessage = field => {
	const defaultMessage = `${field.caption} is required`;
	const message = _.defaultTo(field.[sensitive]RequiredMessage, defaultMessage);
	return makeValidationMessage(field.flattenedId, 'required', message, 'required');
};

/**
 * Makes a validation message for a format mask failure
 * @param {Object} state - Root redux state
 * @param {ValidationField} field - Field to make message for
 * @returns {ValidationMessage}
 */
export const makeFormatValidationMessage = (state, field) => {
	const defaultMessage = getFormatMessage(state, field.maskType, field.maskParameters)(field);
	const message = _.defaultTo(field.[sensitive]FormatMessage, defaultMessage);
	return makeValidationMessage(field.flattenedId, 'format', message);
};

/**
 * Makes a validation message for a blacklist failure
 * @param {ValidationField} field - Field to make message for
 * @returns {ValidationMessage}
 */
export const makeBlacklistValidationMessage = field => {
	const defaultMessage = `${field.caption} cannot be ${field.value}`;
	const message = _.defaultTo(field.[sensitive]BlackListMessage, defaultMessage);
	return makeValidationMessage(field.flattenedId, 'invalid', message);
};

/**
 * Returns whether or not the field value is blacklisted
 * @param {ValidationField} field - The field we are currently validating
 * @return {boolean} - True if the field is black listed, false otherwise
 */
const isFieldValueBlackListed = field => {
	if (!_.has(field, '[sensitive]BlackList')) {
		return false;
	}
	const blackList = field.[sensitive]BlackList;
	const blackListWithoutWhiteSpace = blackList.replace(/\s/g, '');
	const blackListArray = _.split(blackListWithoutWhiteSpace, ',');
	return _.includes(blackListArray, field.value);
};

/**
 * Generate a validation result for the given field
 * @param {ValidationField} field
 * @returns {import('redux-thunk').ThunkAction<ValidationResult, any, void, any>}
 */
export const runValidationForField = field => (dispatch, getState) => {
	/**
	 * Check required status of field
	 */
	if (field.required === '1') {
		const getIsRequiredValid = getIsRequiredValidFunc(
			getState(),
			field.maskType,
			field.maskParameters
		);
		const isRequiredValid = getIsRequiredValid(
			field,
			_.defaultTo(field.maskedValue, field.value)
		);
		if (!isRequiredValid) {
			return {
				flattenedId: field.flattenedId,
				uid: field.uid,
				isValid: false,
				isRequiredValid,
				isFormatValid: false,
				isFieldValueAllowed: false,
				validationMessage: makeRequiredValidationMessage(field),
			};
		}
	}

	/**
	 * Check format mask for field
	 */
	if (!_.isEmpty(field.maskType)) {
		const isFormatValidFunc = getIsFormatValidFunc(
			getState(),
			field.maskType,
			field.maskParameters
		);
		const isFormatValid = isFormatValidFunc(field.value);
		if (!isFormatValid) {
			return {
				flattenedId: field.flattenedId,
				uid: field.uid,
				isValid: false,
				isRequiredValid: true,
				isFormatValid,
				isFieldValueAllowed: false,
				validationMessage: makeFormatValidationMessage(getState(), field),
			};
		}
	}

	/**
	 * Check black list for field
	 */
	const isFieldValueAllowed = !isFieldValueBlackListed(field);
	if (!isFieldValueAllowed) {
		return {
			flattenedId: field.flattenedId,
			uid: field.uid,
			isValid: false,
			isRequiredValid: true,
			isFormatValid: true,
			isFieldValueAllowed,
			isBusinessRulesValid: false,
			validationMessage: makeBlacklistValidationMessage(field),
		};
	}

	/**
	 * All validations passed
	 */
	return {
		flattenedId: field.flattenedId,
		uid: field.uid,
		isValid: true,
		isRequiredValid: true,
		isFormatValid: true,
		isFieldValueAllowed: true,
	};
};

/**
 * @typedef {Object} RunValidationReturn
 * @property {ValidationResult[]} validationResults
 * @property {boolean} allValidationsPassedForPage
 *
 * Runs form validation on the list of given fields
 * @param {string} topic - Topic for given fields
 * @param {string} page - Page for given fields
 * @param {ValidationField[]} fieldList - Field objects to validate
 * @param {boolean} storeResultSet - Whether or not to save the results from this run as validation for the page (this will display any errors to the user)
 * @emits VALIDATION_RUN_RESULTS - Action containing results to be cached in store
 * @returns {import('redux-thunk').ThunkAction<Promise<RunValidationReturn>, any, void, ValidationRunResults>}
 * Resolves to true if all validations passed for the page
 */
export const runValidation = (topic, page, fieldList, storeResultSet) => (dispatch, getState) => {
	// Filter out fields that don't render or are read only
	const filteredFieldList = _.filter(fieldList, field => {
		const shouldFieldRender = getShouldFieldRender(getState(), topic, page, field.uid);
		return shouldFieldRender && !field.isFieldReadOnly;
	});

	let allValidationsPassedForPage = true;
	const validationResults = _.map(filteredFieldList, field => {
		const result = runValidationForField(field)(dispatch, getState);
		if (!result.isValid) allValidationsPassedForPage = false;
		return result;
	});

	dispatch({
		type: VALIDATION_RUN_RESULTS,
		payload: {
			topic,
			page,
			allValidationsPassedForPage,
			validationResults,
			storeResultSet,
		},
	});
	return Promise.resolve({
		validationResults,
		allValidationsPassedForPage,
	});
};
